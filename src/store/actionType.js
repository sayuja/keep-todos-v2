import axios from 'axios';

export const DISPLAY = 'DISPLAY';

export const ADDCARD = 'ADDCARD';

export const CARDADDITION = 'CARDADDITION';

export const EDITCARD = 'EDITCARD';

export const DELETECARD = 'DELETECARD';

const ROOT_URL = 'http://localhost:3000';

export const display = response => {
  return { type: DISPLAY, response: response };
};

export const displayCard = () => {
  return (dispatch, getState) => {
    const request = axios({
      method: 'get',

      url: `${ROOT_URL}/cards`,

      headers: []
    }).then(response => {
      dispatch(display(response.data));
    });
  };
};

export const addCard = cardTitle => {
  return { type: ADDCARD, cards: { cardTitle: cardTitle } };
};

export function cardAddition(cardTitle, id) {
  const payload = {
    id: id,

    cardTitle: cardTitle,

    items: []
  };

  return (dispatch, getState) => {
    const request = axios({
      method: 'post',

      data: payload,

      url: `${ROOT_URL}/cards`
    }).then(response => {
      dispatch(addCard(cardTitle));
    });
  };
}

export const editCard = card => {
  return { type: EDITCARD, cardPayload: { card: card } };
};

export function editCardItems(card) {
  const payload = {
    id: card.id,

    cardTitle: card.title,

    items: card.list,

    color: card.color
  };

  return (dispatch, getState) => {
    const request = axios({
      method: 'put',

      data: payload,

      url: `${ROOT_URL}/cards/${card.id}`
    }).then(response => {
      dispatch(editCard(payload));
    });
  };
}

export const deleteCardAction = id => {
  return { type: DELETECARD, cardData: { id: id } };
};

export function deleteCard(id) {
  return (dispatch, getState) => {
    const request = axios({
      method: 'delete',

      url: `${ROOT_URL}/cards/${id}`
    }).then(response => {
      dispatch(deleteCardAction(id));
    });
  };
}
