import * as actionType from './actionType';

const initiateState = {
  cards: []
};

const reducer = (state = initiateState, action) => {
  switch (action.type) {
    case actionType.DISPLAY:
      //state=action.response;

      return {
        ...state,

        cards: state.cards.concat(action.response)
      };

    case actionType.ADDCARD:
      const card = {
        //id: state.cards.length===0?1:state.cards.length,

        id: state.cards.length + 1,

        cardTitle: action.cards.cardTitle,

        items: [],

        color: 'lightpink'
      };

      return {
        ...state,

        cards: state.cards.concat(card)
      };

    case actionType.EDITCARD:
      const editedCardIndex = state.cards.findIndex(
        card => card.id === action.cardPayload.card.id
      );

      return {
        ...state,
        cards: [
          ...state.cards.slice(0, editedCardIndex),
          action.cardPayload.card,
          ...state.cards.slice(editedCardIndex + 1)
        ]
      };

    case actionType.DELETECARD:
      return {
        ...state,

        cards: state.cards.filter(card => card.id !== action.cardData.id)
      };

    default:
      return state;
  }
};

export default reducer;
