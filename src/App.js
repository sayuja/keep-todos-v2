import React, { Component } from 'react';

import logo from './logo.svg';

import './App.css';

import { connect } from 'react-redux';

import * as actionCreators from './store/actionType';

import AddTodo from './components/AddTodo';

import Header from './components/Header';

//import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

import Editor from './components/Editor';

import Footer from './components/Footer';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import { library } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faTrash, faCircle } from '@fortawesome/free-solid-svg-icons';

/* global location */

/* eslint no-restricted-globals: ["off", "location"] */

class App extends Component {
  constructor(props) {
    super(props);

    library.add(faTrash, faCircle);
  }

  state = {
    isCardEditorOpen: false,

    value: '',

    cardToEdit: {
      id: 1,

      title: '',

      list: [],

      color: 'lightpink'
    }
  };

  handleOnItemChange = newItem => {
    this.state.cardToEdit.list.push(newItem);

    this.setState({
      ...this.state
    });
  };

  handleOnItemEdit = editedItem => {
    //this.state.cardToEdit.list.filter(oldItem).concat(editedItem);

    //this.state.cardToEdit.list[index]=editedItem;

    //this.state.cardToEdit.list.map(itemObj=>itemObj.id===editedItem.id || editedItem);

    let editedItemIndex = this.state.cardToEdit.list.findIndex(
      itemObj => itemObj.id === editedItem.id
    );

    //newList.push(editedItem);

    //this.state.cardToEdit.list.filter(itemObj=>itemObj.id!==editedItem.id).push(editedItem);

    this.setState({
      cardToEdit: {
        id: this.state.cardToEdit.id,

        title: this.state.cardToEdit.title,
        color: this.state.cardToEdit.color,
        list: [
          ...this.state.cardToEdit.list.slice(0, editedItemIndex),
          editedItem,
          ...this.state.cardToEdit.list.slice(editedItemIndex + 1)
        ]
      }
    });

    //this.props.editCardItems(this.state.cardToEdit);
  };

  componentWillMount() {
    this.props.onDisplay();
  }

  handleCardClick = id => {
    const cardToEdit = this.props.cards.filter(card => card.id === id)[0];

    this.setState({
      cardToEdit: {
        id: cardToEdit.id,

        title: cardToEdit.cardTitle,

        list: cardToEdit.items,

        color: cardToEdit.color
      },

      isCardEditorOpen: true
    });
  };

  handleCloseCardEditor = cardToEdit => {
    this.setState({
      isCardEditorOpen: false
    });

    this.props.editCardItems(cardToEdit);
  };

  handleDeleteCard = id => {
    var result = confirm('Want to delete?');

    if (result) {
      //Logic to delete the item

      this.props.deleteCard(id);

      this.setState({
        isCardEditorOpen: false
      });
    }
  };

  handleFavColor = (e, favColor, index, card) => {
    //if (!e) var e = window.event;

    e.cancelBubble = true;

    if (e.stopPropagation) e.stopPropagation();

    const div = document.getElementById(index + 'blockDiv');

    div.style.backgroundColor = favColor;

    card = {
      id: card.id,

      title: card.cardTitle,

      list: card.items,

      color: favColor
    };

    this.props.editCardItems(card);
  };

  selectFavColor = e => {
    e.cancelBubble = true;

    if (e.stopPropagation) e.stopPropagation();
  };

  render() {
    return (
      <div>
        <Header />

        <AddTodo
          addCard={this.props.onAdd}
          noOfCards={this.props.cards.length}
        />

        <React.Fragment>
          {this.props.cards.map((card, index) => (
            <div
              key={index}
              className="blockDiv"
              id={index + 'blockDiv'}
              onClick={() => this.handleCardClick(card.id)}
              style={{ backgroundColor: card.color }}
            >
              <h5 className="title">{card.cardTitle}</h5>

              <ul>
                {card.items.map((item, index) => (
                  <li key={index}>
                    <span className={item.isCompleted ? 'completed' : ''}>
                      <input
                        type="checkbox"
                        id={item.text}
                        checked={item.isCompleted}
                        readOnly
                      />
                      {item.text}
                    </span>
                  </li>
                ))}
              </ul>

              <div className="color-btn">
                {' '}
                <FontAwesomeIcon
                  icon="circle"
                  style={{ color: 'lightpink' }}
                  onClick={e =>
                    this.handleFavColor(e, 'lightpink', index, card)
                  }
                />
                <FontAwesomeIcon
                  icon="circle"
                  style={{ color: '#61dafb' }}
                  onClick={e => this.handleFavColor(e, '#61dafb', index, card)}
                />
                <FontAwesomeIcon
                  icon="circle"
                  style={{ color: 'lightgreen' }}
                  onClick={e =>
                    this.handleFavColor(e, 'lightgreen', index, card)
                  }
                />
              </div>
            </div>
          ))}
        </React.Fragment>

        <Editor
          isCardEditorOpen={this.state.isCardEditorOpen}
          cardToEdit={this.state.cardToEdit}
          handleCloseCardEditor={this.handleCloseCardEditor}
          handleOnItemChange={this.handleOnItemChange}
          handleOnItemEdit={this.handleOnItemEdit}
          handleDeleteCard={this.handleDeleteCard}
        />

        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cards: state.cards
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDisplay: () => dispatch(actionCreators.displayCard()),

    onAdd: (cardTitle, id) =>
      dispatch(actionCreators.cardAddition(cardTitle, id)),

    editCardItems: card => dispatch(actionCreators.editCardItems(card)),

    deleteCard: id => dispatch(actionCreators.deleteCard(id))
  };
};

export default connect(
  mapStateToProps,

  mapDispatchToProps
)(App);
