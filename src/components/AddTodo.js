import React, { Component } from 'react';

import '../App.css';

class AddTodo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cardTitle: ''
    };
  }

  onChangeHandler(e) {
    this.setState({
      cardTitle: e.target.value
    });
  }

  render() {
    return (
      <div id="addTodo">
        <input
          type="text"
          name="todoText"
          id="todoText"
          onChange={e => this.onChangeHandler(e)}
          value={this.state.cardTitle}
          required
        />

        <button
          name="ADD"
          id="ADD"
          onClick={() => {
            if (!this.state.cardTitle.length) {
              return;
            }
            this.props.addCard(this.state.cardTitle, this.props.noOfCards + 1);
          }}
        >
          ADD TODO
        </button>
      </div>
    );
  }
}

export default AddTodo;
