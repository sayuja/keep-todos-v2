import React, { Fragment, Component } from 'react';

import AddTodoItem from './AddTodoItem';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Button } from 'react-bootstrap';

export default class Editor extends Component {
  constructor(props) {
    super(props);

    this.props = props;

    this.state = {
      isItemAdd: false,

      // newItem:'',

      cardToEdit: this.props.cardToEdit

      // itemName:''
    };
  }

  onItemAddClickHandle = () => {
    this.setState({
      isItemAdd: true
    });
  };

  onItemChangeHandle = (newItem, showTextBox) => {
    // this.props.cardToEdit.list.push(newItem);

    const newItemObj = {
      id: this.props.cardToEdit.list.length + 1,

      text: newItem,

      isCompleted: ''
    };

    this.setState({
      isItemAdd: showTextBox
    });

    this.props.handleOnItemChange(newItemObj);
  };

  handleEditItem = (e, itemToEdit) => {
    let editedItem;

    var id = e.currentTarget.id;

    if (id.includes('chk')) {
      id = id.replace('-chk', '-li');

      editedItem = {
        id: itemToEdit.id,

        text: document.getElementById(id).innerText,

        isCompleted: e.target.checked,

        color: itemToEdit.color
      };
    } else {
      id = id.replace('-li', '-chk');

      editedItem = {
        id: itemToEdit.id,

        text: e.target.innerText,

        isCompleted: document.getElementById(id).checked,
        color: itemToEdit.color
      };
    }

    this.props.handleOnItemEdit(editedItem);
  };

  render() {
    if (!this.props.isCardEditorOpen) {
      return null;
    }

    return (
      <React.Fragment>
        <div
          className="popup-background"
          onClick={() =>
            this.props.handleCloseCardEditor(this.props.cardToEdit)
          }
        />

        <div className="popup">
          <div className="popup-title-container">
            <h5>{this.props.cardToEdit.title}</h5>

            <Button
              className="delete-btn btn btn-default"
              onClick={() =>
                this.props.handleDeleteCard(this.props.cardToEdit.id)
              }
            >
              <FontAwesomeIcon icon="trash" style={{ color: 'Red' }} />
            </Button>
          </div>

          <div className="popup-body">
            <div className="list-container">
              <ul>
                {this.props.cardToEdit.list.map((listItem, index) => (
                  <li key={index + '-list'}>
                    <input
                      type="checkbox"
                      id={index + '-chk'}
                      checked={listItem.isCompleted}
                      onChange={e => this.handleEditItem(e, listItem)}
                    />

                    <label
                      className="editor-itemLabel"
                      id={index + '-li'}
                      contenteditable="true"
                      onBlur={e => this.handleEditItem(e, listItem)}
                    >
                      {listItem.text}
                    </label>
                  </li>
                ))}

                {this.state.isItemAdd ? (
                  <AddTodoItem onItemChangeHandle={this.onItemChangeHandle} />
                ) : null}

                <hr />

                <li
                  className="todo-list"
                  key={this.props.cardToEdit.list.length + 1}
                  onClick={() => this.onItemAddClickHandle()}
                >
                  + List item
                </li>
              </ul>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
