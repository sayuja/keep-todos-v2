import React from 'react';

class AddTodoItem extends React.Component {
  state = {
    itemName: ''
  };

  onChangeHandler = e => {
    this.setState({ itemName: e.target.value });
  };

  render() {
    var showTextBox = false;

    return (
      <div>
        <input
          type="text"
          id="item"
          placeholder="Enter ToDo item"
          onChange={e => this.onChangeHandler(e)}
          value={this.state.item}
          onBlur={() => {
            this.props.onItemChangeHandle(this.state.itemName, showTextBox);
          }}
        />
      </div>
    );
  }
}

export default AddTodoItem;
